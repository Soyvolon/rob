﻿using DSharpPlus;
using DSharpPlus.Commands;
using DSharpPlus.Commands.ContextChecks;
using DSharpPlus.Entities;

using ROBBot.Services.Isekai;
using ROBBot.Services.Module;

using ROBBot.Structures.Discord;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Commands.Slots;

public partial class SlotsCommandGroup
{
	[Command("toggle")]
	[Description("Toggles the slots module.")]
    public async Task ToggleIsekaiSystem(CommandContext ctx,
		[Description("If true, the module is enabled. Defaults to true.")]
		bool status = true)
	{
		await ctx.DeferResponseAsync();

		if (ctx.Guild is null)
		{
			await ctx.FollowupAsync("This command can only be run in a guild.");
			return;
		}

		var res = await moduleService.ChangeModuleStatusAsync(ctx.Guild.Id, GuildModules.Slots, status);

		if (!res.GetResult(out var err))
        {
            await RespondErrorAsync(ctx, err);
            return;
		}

		var msg = EmpherialBuilder()
			.WithContent($"Slots module enabled status is now: {Formatter.InlineCode(status.ToString())}");

		await ctx.FollowupAsync(msg);
	}

    [Command("status")]
    [Description("Views the status of the module.")]
    [RequirePermissions(DiscordPermissions.None, DiscordPermissions.ManageMessages)]
    public async Task SlotsSystemStatusAsync(CommandContext ctx)
    {
        await ctx.DeferResponseAsync();

        if (ctx.Guild is null)
        {
            await ctx.FollowupAsync("This command can only be run in a guild.");
            return;
        }

        var res = await slotsService.GetSettingsAsync(ctx.Guild.Id);

        if (!res.GetResult(out var err, out var cfg))
        {
            await RespondErrorAsync(ctx, err);
            return;
        }

        var msg = EmpherialBuilder()
            .WithContent($"Slots module status:" +
            $"\nEnabled: {Formatter.InlineCode(cfg.enabled.ToString())}");

        await ctx.FollowupAsync(msg);
    }
}
