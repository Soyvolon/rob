﻿using DSharpPlus.Commands;
using DSharpPlus.Commands.ContextChecks;
using DSharpPlus.Entities;

using ROBBot.Services.Module;
using ROBBot.Services.Slots;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Commands.Slots;

[Command("slots")]
[RequirePermissions(DiscordPermissions.None, DiscordPermissions.ManageMessages)]
public partial class SlotsCommandGroup(
		IModuleService moduleService,
		ISlotsService slotsService
	) : CommandModule
{

}
