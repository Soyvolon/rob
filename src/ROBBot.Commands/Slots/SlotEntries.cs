﻿using DSharpPlus.Commands;
using DSharpPlus.Commands.ContextChecks;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;

using ROBBot.Commands.Converters.Emoji;
using ROBBot.Services.Slots;

using System.ComponentModel;

namespace ROBBot.Commands.Slots;
public partial class SlotsCommandGroup
{
    [Command("entry")]
    [RequirePermissions(DiscordPermissions.None, DiscordPermissions.ManageMessages)]
    public class SlotEntries(
            ISlotsService slots
        )
    {
        [Command("add")]
        [Description("Adds a new slot entry.")]
        [RequirePermissions(DiscordPermissions.None, DiscordPermissions.ManageMessages)]
        public async Task AddEntry(CommandContext ctx,
            [Description("The emoji to display.")]
            EmojiParameter emoji,
            [Description("The weight of this value - higher weight means it shows up more often.")]
            int weight,
            [Description("Message to display when the entry is given.")]
            string onSuccessMessage,
            [Description("Message to display when the entry is not given (due to per errors or something).")]
            string onFailMessage,
            [Description("If true, mark this as a wildcard.")]
            bool wildcard)
        {
            await ctx.DeferResponseAsync();

            var res = await slots.AddEntryAsync(ctx.Guild!.Id, emoji, weight, onSuccessMessage, onFailMessage, wildcard);

            if (!res.GetResult(out var err))
            {
                await RespondErrorAsync(ctx, err);
                return;
            }

            var msg = EmpherialBuilder()
                .WithContent($"Entry added successful using emoji: {emoji}");

            await ctx.FollowupAsync(msg);
        }

        [Command("remove")]
        [Description("Removes a entry.")]
        [RequirePermissions(DiscordPermissions.None, DiscordPermissions.ManageMessages)]
        public async Task RemoveEntryAsync(CommandContext ctx, string emoji)
        {
            await ctx.DeferResponseAsync();

            var res = await slots.RemoveEntryAsync(ctx.Guild!.Id, emoji);

            if (!res.GetResult(out var err))
            {
                await RespondErrorAsync(ctx, err);
                return;
            }

            var msg = EmpherialBuilder()
                .WithContent($"Entry removed successful using emoji: {emoji}");

            await ctx.FollowupAsync(msg);
        }

        [Command("list")]
        [Description("Lists all entries.")]
        [RequirePermissions(DiscordPermissions.None, DiscordPermissions.ManageMessages)]
        public async Task ListEntriesAsync(CommandContext ctx)
        {

            await ctx.DeferResponseAsync();

            var res = await slots.GetAllEntriesAsync(ctx.Guild!.Id);

            if (!res.GetResult(out var err, out var entries))
            {
                await RespondErrorAsync(ctx, err);
                return;
            }

            List<Page> pages = [];
            for (int pageCount = 0; pageCount < entries.Count; pageCount += 10)
            {
                var embed = new DiscordEmbedBuilder();
                for (int i = pageCount; i < entries.Count; i++) 
                {
                    var data = entries[i];
                    embed.AddField(data.Emote, data.GetEntryInfo());
                }

                var page = new Page(embed: embed);
                pages.Add(page);
            }

            if (pages.Count == 0)
            {
                var msg = EmpherialBuilder()
                    .WithContent("There are not slot entries.");

                await ctx.FollowupAsync(msg);
                return;
            }

            await ctx.FollowupAsync("Displaying entries:");
            await ctx.Channel.SendPaginatedMessageAsync(ctx.User, pages);
        }
    }
}
