﻿using DSharpPlus.Commands;
using DSharpPlus.Commands.ContextChecks;
using DSharpPlus.Entities;

using ROBBot.Commands.Converters.Emoji;
using ROBBot.Services.Slots;
using ROBBot.Structures.Migrations;
using ROBBot.Structures.Slots;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Commands.Slots;
public partial class SlotsCommandGroup
{
    [Command("reward")]
    [RequirePermissions(DiscordPermissions.None, DiscordPermissions.ManageMessages)]
    public class SlotRewards(
            ISlotsService slots
        )
    {
        [Command("role")]
        [Description("Sets the reward to toggling a role.")]
        [RequirePermissions(DiscordPermissions.None, DiscordPermissions.ManageMessages)]
        public async Task SetRoleReward(CommandContext ctx, EmojiParameter emoji, DiscordRole roleToToggle)
        {
            await ctx.DeferResponseAsync();

            var res = await slots.AddRewardAsync(ctx.Guild!.Id, emoji, RewardType.Role, roleToToggle.Id);

            if (!res.GetResult(out var err))
            {
                await RespondErrorAsync(ctx, err);
                return;
            }

            var msg = EmpherialBuilder()
                .WithContent("Reward changed to Role successfully");

            await ctx.FollowupAsync(msg);
        }

        [Command("nickname")]
        [Description("Sets the reward to granting a nickname.")]
        [RequirePermissions(DiscordPermissions.None, DiscordPermissions.ManageMessages)]
        public async Task SetNicknameReward(CommandContext ctx, EmojiParameter emoji, string nicknameToSet)
        {
            await ctx.DeferResponseAsync();

            var res = await slots.AddRewardAsync(ctx.Guild!.Id, emoji, RewardType.Nickname, nicknameToSet);

            if (!res.GetResult(out var err))
            {
                await RespondErrorAsync(ctx, err);
                return;
            }

            var msg = EmpherialBuilder()
                .WithContent("Reward changed to Nickname successfully");

            await ctx.FollowupAsync(msg);
        }

        [Command("timeout")]
        [Description("Sets the reward to timing someone out.")]
        [RequirePermissions(DiscordPermissions.None, DiscordPermissions.ManageMessages)]
        public async Task SetTimeoutReward(CommandContext ctx, EmojiParameter emoji, TimeSpan timeout)
        {

            await ctx.DeferResponseAsync();

            var res = await slots.AddRewardAsync(ctx.Guild!.Id, emoji, RewardType.Timeout, timeout);

            if (!res.GetResult(out var err))
            {
                await RespondErrorAsync(ctx, err);
                return;
            }

            var msg = EmpherialBuilder()
                .WithContent("Reward changed to Timeout successfully");

            await ctx.FollowupAsync(msg);
        }

        [Command("disconnect")]
        [Description("Sets the reward to disconnecting someone from VC.")]
        [RequirePermissions(DiscordPermissions.None, DiscordPermissions.ManageMessages)]
        public async Task SetDisconnectReward(CommandContext ctx, EmojiParameter emoji)
        {

            await ctx.DeferResponseAsync();

            var res = await slots.AddRewardAsync(ctx.Guild!.Id, emoji, RewardType.Disconnect);

            if (!res.GetResult(out var err))
            {
                await RespondErrorAsync(ctx, err);
                return;
            }

            var msg = EmpherialBuilder()
                .WithContent("Reward changed to Disconnect successfully");

            await ctx.FollowupAsync(msg);
        }

        [Command("kick")]
        [Description("Sets the reward to kicking someone from the server. Will send an invite link to them first.")]
        [RequirePermissions(DiscordPermissions.None, DiscordPermissions.ManageMessages)]
        public async Task SetKickReward(CommandContext ctx, EmojiParameter emoji, string preKickMessage)
        {

            await ctx.DeferResponseAsync();

            var res = await slots.AddRewardAsync(ctx.Guild!.Id, emoji, RewardType.Kick, preKickMessage);

            if (!res.GetResult(out var err))
            {
                await RespondErrorAsync(ctx, err);
                return;
            }

            var msg = EmpherialBuilder()
                .WithContent("Reward changed to Kick successfully");

            await ctx.FollowupAsync(msg);
        }
    }
}
