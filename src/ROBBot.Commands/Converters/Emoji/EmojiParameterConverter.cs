﻿using DSharpPlus.Commands.Processors.SlashCommands;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ROBBot.Commands.Converters.Emoji;
public partial class EmojiParameterConverter : ISlashArgumentConverter<EmojiParameter>
{
    public DiscordApplicationCommandOptionType ParameterType => DiscordApplicationCommandOptionType.String;

    public string ReadableName => "emoji";

    public Task<Optional<EmojiParameter>> ConvertAsync(InteractionConverterContext context, InteractionCreatedEventArgs eventArgs)
    {
        var emojiRaw = context.Argument?.RawValue ?? "";

        if (DiscordEmoji.TryFromUnicode(emojiRaw, out DiscordEmoji emoji))
        {
            return Task.FromResult(Optional.FromValue(new EmojiParameter
            {
                DiscordEmoji = emoji,
                CustomEmoji = null,
                IsCustom = false
            }));
        }
        else if (CustomEmojiRegex().Match(emojiRaw).Success)
        {
            var idRaw = emojiRaw.Split(':')[2].Trim('>');
            if (ulong.TryParse(idRaw, out var id))
            {
                if (DiscordEmoji.TryFromGuildEmote(context.Client, id, out emoji))
                {
                    return Task.FromResult(Optional.FromValue(new EmojiParameter
                    {
                        DiscordEmoji = emoji,
                        CustomEmoji = null,
                        IsCustom = false
                    }));
                }
            }

            return Task.FromResult(Optional.FromValue(new EmojiParameter
            {
                DiscordEmoji = null,
                CustomEmoji = emojiRaw,
                IsCustom = true
            }));
        }

        return Task.FromResult(Optional.FromNoValue<EmojiParameter>());
    }

    [GeneratedRegex("^<:\\S*:\\d*>$")]
    private static partial Regex CustomEmojiRegex();
}
