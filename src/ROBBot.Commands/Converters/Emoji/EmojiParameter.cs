﻿using DSharpPlus.Entities;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Commands.Converters.Emoji;
public struct EmojiParameter
{
    public DiscordEmoji? DiscordEmoji;
    public string? CustomEmoji;
    public bool IsCustom;

    public override string ToString()
        => IsCustom
            ? CustomEmoji!
            : DiscordEmoji!;

    public static implicit operator string(EmojiParameter value)
        => value.ToString();
}
