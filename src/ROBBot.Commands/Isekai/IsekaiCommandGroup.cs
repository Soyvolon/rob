﻿using DSharpPlus.Commands;
using DSharpPlus.Commands.ContextChecks;
using DSharpPlus.Entities;

using ROBBot.Services.Isekai;
using ROBBot.Services.Module;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Commands.Isekai;

[Command("isekai")]
[RequirePermissions(DiscordPermissions.None, DiscordPermissions.ManageMessages)]
public partial class IsekaiCommandGroup(
		IModuleService moduleService,
		IIsekaiService isekaiService
	) : CommandModule
{

}
