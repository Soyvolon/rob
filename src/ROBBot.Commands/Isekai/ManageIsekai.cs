﻿using DSharpPlus;
using DSharpPlus.Commands;
using DSharpPlus.Entities;

using ROBBot.Structures.Discord;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Commands.Isekai;
public partial class IsekaiCommandGroup
{
	[Command("toggle")]
	[Description("Toggles the isekai module.")]
	public async Task ToggleIsekaiSystemAsync(CommandContext ctx,
		[Description("If true, the module is enabled. Defaults to true.")]
		bool status = true)
	{
		await ctx.DeferResponseAsync();

		if (ctx.Guild is null)
		{
			await ctx.FollowupAsync("This command can only be run in a guild.");
			return;
		}

		var res = await moduleService.ChangeModuleStatusAsync(ctx.Guild.Id, GuildModules.Isekai, status);

		if (!res.GetResult(out var err))
		{
			await RespondErrorAsync(ctx, err);
			return;
		}

		var msg = EmpherialBuilder()
			.WithContent($"Isekai module enabled status is now: {Formatter.InlineCode(status.ToString())}");

		await ctx.FollowupAsync(msg);
	}

	[Command("configure")]
	[Description("Configures the Isekai system.")]
	public async Task ConfigureIsekaiSystemAsync(CommandContext ctx,
		[Description("The time between triggers.")]
		TimeSpan? triggerTimer = null,
		[Description("The randomness value added to the trigger timer.")]
		float? randomness = null,
		[Description("A comma separated list of names to use. Use clearnames command to clear.")]
		string? nameList = null)
	{
		await ctx.DeferResponseAsync();

		if (ctx.Guild is null)
		{
			await ctx.FollowupAsync("This command can only be run in a guild.");
			return;
		}

		var res = await isekaiService.UpdateSettingsAsync(ctx.Guild.Id, e =>
		{
			if (triggerTimer is not null)
				e.TriggerTimer = triggerTimer.Value;

			if (randomness is not null)
				e.Randomness = randomness.Value;

			if (nameList is not null)
				e.NameList = [.. nameList.Split(",").Select(e => e.Trim())];
		});

		if (!res.GetResult(out var err))
        {
            await RespondErrorAsync(ctx, err);
            return;
		}

		var msg = EmpherialBuilder()
			.WithContent($"Isekai module status updated.");

		await ctx.FollowupAsync(msg);
	}

	[Command("clearnames")]
	[Description("Clears the configured names for the module.")]
	public async Task ConfigureIsekaiSystemAsync(CommandContext ctx)
	{
		await ctx.DeferResponseAsync();

		if (ctx.Guild is null)
		{
			await ctx.FollowupAsync("This command can only be run in a guild.");
			return;
		}

		var res = await isekaiService.UpdateSettingsAsync(ctx.Guild.Id, e =>
		{
			e.NameList.Clear();
		});

		if (!res.GetResult(out var err))
        {
            await RespondErrorAsync(ctx, err);
            return;
		}

		var msg = EmpherialBuilder()
			.WithContent($"Isekai module status updated.");

		await ctx.FollowupAsync(msg);
	}

	[Command("status")]
	[Description("Views the status of the module.")]
	public async Task IsekaiSystemStatusAsync(CommandContext ctx)
	{
		await ctx.DeferResponseAsync();

		if (ctx.Guild is null)
		{
			await ctx.FollowupAsync("This command can only be run in a guild.");
			return;
		}

		var res = await isekaiService.GetSettingsAsync(ctx.Guild.Id);

		if (!res.GetResult(out var err, out var cfg))
        {
            await RespondErrorAsync(ctx, err);
            return;
		}

		var msg = EmpherialBuilder()
			.WithContent($"Isekai module status:" +
			$"\nEnabled: {Formatter.InlineCode(cfg.enabled.ToString())}" +
			$"\n\n--- Settings ---\n" +
			$"\nTrigger Timer (minutes): {Formatter.InlineCode(cfg.cfg.TriggerTimer.TotalMinutes.ToString())}" +
			$"\nRandomness: {Formatter.InlineCode(cfg.cfg.Randomness.ToString())}" +
			$"\nName List: {Formatter.InlineCode(string.Join(", ", cfg.cfg.NameList))}" +
			$"\n\n--- Status ---" +
			$"\nLast Trigger: {Formatter.InlineCode(cfg.cfg.LastTrigger.ToString("s"))}");

		await ctx.FollowupAsync(msg);
	}
}
