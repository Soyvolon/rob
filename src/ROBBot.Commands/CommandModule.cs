﻿

using DSharpPlus.Commands;
using DSharpPlus.Entities;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Commands;
public class CommandModule
{
	protected static DiscordFollowupMessageBuilder EmpherialBuilder()
		=> new DiscordFollowupMessageBuilder()
			.AsEphemeral();

	protected static async Task RespondErrorAsync(CommandContext ctx, List<string> err)
	{
        await ctx.FollowupAsync(EmpherialBuilder().WithContent(err[0]));
    }
}