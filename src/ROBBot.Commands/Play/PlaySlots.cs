﻿using DSharpPlus.Commands;
using DSharpPlus.Entities;
using ROBBot.Services.Slots;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Commands.Play;
public partial class PlayCommandGroup
{
    private readonly TimeSpan _slotDelay = TimeSpan.FromSeconds(1);

    [Command("slots")]
    [Description("Play slots.")]
    public async Task PlaySlotsAsync(CommandContext ctx)
    {
        await ctx.DeferResponseAsync();

        var res = await slotsService.RollSlotsAsync(ctx.Guild!.Id);

        if (!res.GetResult(out var err, out var roll))
        {
            await RespondErrorAsync(ctx, err);
            return;
        }

        const string msgFormat = "{0}{1}{2}";

        var content = string.Format(msgFormat, roll.System.RollingEmote,
            roll.System.RollingEmote, roll.System.RollingEmote);

        var msg = new DiscordMessageBuilder()
            .WithContent(content);

        var sentMsg = await ctx.FollowupAsync(msg);

        for (int i = 0; i < 3; i++)
        {
            await Task.Delay(_slotDelay);

            content = string.Format(msgFormat, roll.First,
                i >= 1 ? roll.Second : roll.System.RollingEmote,
                i >= 2 ? roll.Third : roll.System.RollingEmote);

            msg.WithContent(content);

            await sentMsg.ModifyAsync(msg);
        }

        if (!roll.IsWin)
        {
            await ctx.Channel.SendMessageAsync(roll.System.NiceTryMessage);
            return;
        }

        var reward = roll.Reward!;
        var member = ctx.Member;

        if (member is null)
        {
            await ctx.Channel.SendMessageAsync("Member is null so I can't provide rewards!");
            return;
        }

        try
        {
            switch (reward.RewardType)
            {
                case Structures.Slots.RewardType.Role:
                    var role = member.Roles.FirstOrDefault(e => e.Id == reward.RoleToggled);
                    if (role is not null)
                        await member.RevokeRoleAsync(role, "Slots!");
                    else
                    {
                        var newRole = ctx.Guild.Roles.Values.FirstOrDefault(e => e.Id == reward.RoleToggled);
                        if (newRole is null)
                        {
                            await ctx.Channel.SendMessageAsync("The role we tried to give you does not exist!");
                            return;
                        }

                        await member.GrantRoleAsync(newRole, "Slots!");
                    }
                    break;

                case Structures.Slots.RewardType.Nickname:
                    await member.ModifyAsync(e => e.Nickname = reward.NicknameGranted);
                    break;

                case Structures.Slots.RewardType.Timeout:
                    await member.TimeoutAsync(DateTime.UtcNow + TimeSpan.FromSeconds(reward.TimeoutSeconds), "Slots!");
                    break;

                case Structures.Slots.RewardType.Disconnect:
                    await member.ModifyAsync(e => e.VoiceChannel = Optional.FromValue<DiscordChannel>(null!));
                    break;

                case Structures.Slots.RewardType.Kick:
                    var dm = await member.CreateDmChannelAsync();
                    await dm.SendMessageAsync(reward.KickMessage);

                    await member.RemoveAsync("Slots!");
                    break;
            }
        }
        catch (Exception ex)
        {
            await ctx.Channel.SendMessageAsync(reward.OnFailureMessage);
            return;
        }

        await ctx.Channel.SendMessageAsync(reward.OnSuccessMessage);
    }
}
