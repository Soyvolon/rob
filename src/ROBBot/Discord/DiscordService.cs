﻿using DSharpPlus.Interactivity;
using DSharpPlus;
using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.EventArgs;
using Microsoft.Extensions.Logging;

using DSharpPlus.Commands.Processors.SlashCommands;
using DSharpPlus.Commands;
using ROBBot.Commands;
using ROBBot.Services.Isekai;

namespace ROBBot.Discord;
public class DiscordService : IDiscordService
{
	private readonly IConfiguration _configuration;
	private readonly DiscordClient _client;
	private readonly IIsekaiService _isekaiService;

	public DiscordService(IConfiguration configuration, DiscordClient client, 
		IIsekaiService isekaiService, IServiceProvider services)
	{
		_configuration = configuration;
		_isekaiService = isekaiService;
		_client = client;
	}

	public async Task InitializeAsync()
	{
		await _client.ConnectAsync();

		// do service init
		await _isekaiService.InitAsync();
	}
}
