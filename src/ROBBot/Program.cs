﻿using DSharpPlus;
using DSharpPlus.Commands;
using DSharpPlus.Commands.Processors.SlashCommands;
using DSharpPlus.Extensions;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Enums;
using DSharpPlus.Interactivity.Extensions;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using ROBBot.Commands;
using ROBBot.Discord;
using ROBBot.Events;
using ROBBot.Services.Isekai;
using ROBBot.Services.Module;
using ROBBot.Services.Slots;
using ROBBot.Structures.Database;

using Serilog;

namespace ROBBot;

internal class Program
{
	static async Task<int> Main(string[] args)
	{
		IConfiguration config = new ConfigurationBuilder()
			.SetBasePath(Directory.GetCurrentDirectory())
			.AddJsonFile(Path.Combine(Environment.CurrentDirectory, "Data", "config.json"))
			.Build();

		IServiceCollection services = new ServiceCollection();
		services.AddSingleton(config);
		services.AddLogging(builder =>
		{
			var logger = new LoggerConfiguration()
				.ReadFrom.Configuration(config)
				.CreateLogger();

			builder.AddSerilog(logger);
		});

		services.AddDbContextFactory<ApplicationDbContext>(options =>
			options.UseSqlite(config.GetConnectionString("Core"))
				#if DEBUG
				.EnableSensitiveDataLogging()
				.EnableDetailedErrors()
				#endif
				, ServiceLifetime.Singleton);

		var commandConfig = new CommandsConfiguration()
		{
			RegisterDefaultCommandProcessors = false,
		};

        if (ulong.TryParse(config["Discord:DebugGuild"], out var debugGuild))
            commandConfig.DebugGuildId = debugGuild;

		var processor = new SlashCommandProcessor()
		{

		};

		services.AddShardedDiscordClient(config["Discord:Token"]
                ?? throw new Exception("Missing bot token"),
                SlashCommandProcessor.RequiredIntents | DiscordIntents.GuildMembers)
            .AddCommandsExtension(setup =>
            {
                if (ulong.TryParse(config["Discord:DebugGuild"], out var debugGuild))
                    setup.AddCommands(typeof(CommandModule).Assembly, debugGuild);
                else setup.AddCommands(typeof(CommandModule).Assembly);
				
				processor.AddConverters(typeof(CommandModule).Assembly);

                setup.AddProcessor(processor);
            }, commandConfig)
            .AddInteractivityExtension(new()
            {
                ButtonBehavior = ButtonPaginationBehavior.DeleteButtons,
                PaginationBehaviour = PaginationBehaviour.WrapAround,
				PollBehaviour = PollBehaviour.DeleteEmojis,
            })
            .ConfigureEventHandlers(cfg =>
            {
                cfg.AddEventHandlers<ClientErroredEventHandler>(ServiceLifetime.Singleton)
                    .AddEventHandlers<GuildCreatedEventHandler>(ServiceLifetime.Singleton);
            });

        services.AddSingleton<IDiscordService, DiscordService>();

		services.AddScoped<IModuleService, ModuleService>()
            .AddScoped<IIsekaiService, IsekaiService>()
            .AddScoped<ISlotsService, SlotsService>();

		var provider = services.BuildServiceProvider();

		await RunAsync(provider);

		// never exit.
		await Task.Delay(-1);

		return 0;
	}

	static async Task RunAsync(IServiceProvider services)
	{
		using var scope = services.CreateScope();
		var logger = scope.ServiceProvider.GetRequiredService<ILogger<Program>>();
		var dbFac = scope.ServiceProvider.GetRequiredService<IDbContextFactory<ApplicationDbContext>>();
		using var db = dbFac.CreateDbContext();
		await ApplyDatabaseMigrationsAsync(db);

		var discordClient = scope.ServiceProvider.GetRequiredService<IDiscordService>();
		_ = Task.Run(async () => {
			try
			{
				await discordClient.InitializeAsync();
			}
			catch (Exception ex)
			{
				logger.LogError(ex, "Failed to startup discord.");
			}
		});

		logger.LogInformation("Bot started.");
	}

	private static async Task ApplyDatabaseMigrationsAsync(DbContext database)
	{
		if (!(await database.Database.GetPendingMigrationsAsync()).Any())
		{
			return;
		}

		await database.Database.MigrateAsync();
		await database.SaveChangesAsync();
	}
}
