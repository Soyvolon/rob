﻿using DSharpPlus;
using DSharpPlus.EventArgs;

using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Events;
public class ClientErroredEventHandler : IEventHandler<ClientErrorEventArgs>
{
    public Task HandleEventAsync(DiscordClient sender, ClientErrorEventArgs eventArgs)
    {
        sender.Logger.LogError(eventArgs.Exception, "Client errored");

        return Task.CompletedTask;
    }
}
