
using ROBBot.Structures.Discord;
using ROBBot.Structures.Result;
using ROBBot.Structures.Slots;

namespace ROBBot.Services.Slots;

/// <summary>
/// Template Description
/// </summary>
public interface ISlotsService
{
    public Task<ActionResult<(bool enabled, SlotsSystem cfg)>> GetSettingsAsync(ulong guild);
    public Task<ActionResult> AddEntryAsync(ulong guild, string emoji, int weight, string onSuccess, string onFail, bool wildcard);
    public Task<ActionResult> AddRewardAsync(ulong guild, string emoji, RewardType type, params object[] configs);
    public Task<ActionResult> RemoveEntryAsync(ulong guild, string emoji);
    public Task<ActionResult<List<SlotEntry>>> GetAllEntriesAsync(ulong guild);
    public Task<ActionResult<SlotResult>> RollSlotsAsync(ulong guild);
}
