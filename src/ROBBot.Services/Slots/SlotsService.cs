﻿using DSharpPlus.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using ROBBot.Structures.Database;
using ROBBot.Structures.Discord;
using ROBBot.Structures.Result;
using ROBBot.Structures.Slots;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ROBBot.Services.Slots;
/// <summary>
/// Template Description
/// </summary>
public partial class SlotsService(
        IDbContextFactory<ApplicationDbContext> dbContextFactory,
        ILogger<SlotsService> logger
    ) : ISlotsService
{
    public async Task<ActionResult> AddEntryAsync(ulong guild, string emoji, int weight, string onSuccess, string onFail, bool wildcard)
    {
        try
        {
            await using var dbContext = await dbContextFactory.CreateDbContextAsync();

            var guildConfig = await dbContext.GetGuildConfigurationAsync(guild);

            var entry = await dbContext.SlotEntries
                .Where(e => e.SystemKey == guildConfig.SlotsSystem.Key)
                .Where(e => e.Emote == emoji)
                .FirstOrDefaultAsync();

            if (entry is null)
            {
                entry = new SlotEntry();
                guildConfig.SlotsSystem.SlotEntries.Add(entry);
            }

            entry.Emote = emoji;
            entry.IsWildcard = wildcard;
            entry.Weight = weight;
            entry.OnSuccessMessage = onSuccess;
            entry.OnFailureMessage = onFail;

            await dbContext.SaveChangesAsync();

            return new(true);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Unexpected error in {method}", nameof(AddEntryAsync));
            return new(false, [$"An unexpected error occurred in {nameof(AddEntryAsync)}", ex.Message]);
        }
    }

    public async Task<ActionResult> AddRewardAsync(ulong guild, string emoji, RewardType type, params object[] configs)
    {
        try
        {
            await using var dbContext = await dbContextFactory.CreateDbContextAsync();

            var guildConfig = await dbContext.GetGuildConfigurationAsync(guild);

            var entry = await dbContext.SlotEntries
                .Where(e => e.SystemKey == guildConfig.SlotsSystem.Key)
                .Where(e => e.Emote == emoji)
                .FirstOrDefaultAsync();

            if (entry is null)
                return new(false, [$"Failed to get an entry that uses the following emote: {emoji}"]);

            entry.RewardType = type;
            switch (type)
            {
                case RewardType.Role:
                    entry.RoleToggled = (ulong)configs[0];
                    break;
                case RewardType.Nickname:
                    entry.NicknameGranted = (string)configs[0];
                    break;
                case RewardType.Timeout:
                    entry.TimeoutSeconds = (int)((TimeSpan)configs[0]).TotalSeconds;
                    break;
                case RewardType.Disconnect:
                    // no config
                    break;
                case RewardType.Kick:
                    entry.KickMessage = (string)configs[0];
                    break;
            }

            await dbContext.SaveChangesAsync();

            return new(true);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Unexpected error in {method}", nameof(AddRewardAsync));
            return new(false, [$"An unexpected error occurred in {nameof(AddRewardAsync)}", ex.Message]);
        }
    }

    public async Task<ActionResult<List<SlotEntry>>> GetAllEntriesAsync(ulong guild)
    {
        try
        {
            await using var dbContext = await dbContextFactory.CreateDbContextAsync();

            var guildConfig = await dbContext.GetGuildConfigurationAsync(guild);

            var entries = await dbContext.SlotEntries
                .Where(e => e.SystemKey == guildConfig.SlotsSystem.Key)
                .OrderBy(e => e.Emote)
                .ToListAsync();

            return new(true, null, entries);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Unexpected error in {method}", nameof(GetAllEntriesAsync));
            return new(false, [$"An unexpected error occurred in {nameof(GetAllEntriesAsync)}", ex.Message]);
        }
    }

    public async Task<ActionResult<(bool enabled, SlotsSystem cfg)>> GetSettingsAsync(ulong guild)
    {
        try
        {
            await using var dbContext = await dbContextFactory.CreateDbContextAsync();

            var guildConfig = await dbContext.GetGuildConfigurationAsync(guild);

            return new(true, null, (
                guildConfig.EnabledModules.HasFlag(GuildModules.Slots),
                guildConfig.SlotsSystem
            ));
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Unexpected error in {method}", nameof(GetSettingsAsync));
            return new(false, [$"An unexpected error occurred in {nameof(GetSettingsAsync)}", ex.Message]);
        }
    }

    public async Task<ActionResult> RemoveEntryAsync(ulong guild, string emoji)
    {
        try
        {
            await using var dbContext = await dbContextFactory.CreateDbContextAsync();

            var guildConfig = await dbContext.GetGuildConfigurationAsync(guild);

            await dbContext.SlotEntries
                .Where(e => e.SystemKey == guildConfig.SlotsSystem.Key)
                .Where(e => e.Emote == emoji)
                .ExecuteDeleteAsync();

            return new(true);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Unexpected error in {method}", nameof(RemoveEntryAsync));
            return new(false, [$"An unexpected error occurred in {nameof(RemoveEntryAsync)}", ex.Message]);
        }
    }

    public async Task<ActionResult<SlotResult>> RollSlotsAsync(ulong guild)
    {
        try
        {
            await using var dbContext = await dbContextFactory.CreateDbContextAsync();

            var guildConfig = await dbContext.GetGuildConfigurationAsync(guild);

            if (!guildConfig.EnabledModules.HasFlag(GuildModules.Slots))
                return new(false, ["The slots module is not enabled."]);

            var entries = dbContext.SlotEntries
                .Where(e => e.SystemKey == guildConfig.SlotsSystem.Key)
                .AsAsyncEnumerable();

            List<SlotEntry> entryData = [];
            await foreach(var entry in entries)
            {
                for (int i = 0; i < entry.Weight; i++)
                    entryData.Add(entry);
            }

            SlotEntry[] rolls = new SlotEntry[3];
            for(int i = 0; i < rolls.Length; i++)
            {
                rolls[i] = entryData[Random.Shared.Next(entryData.Count)];
            }

            var first = rolls.FirstOrDefault(e => !e.IsWildcard);
            bool win = rolls.All(e => e == first || e.IsWildcard);

            if (!win)
            {
                return new(true, null, new()
                {
                    IsWin = false,
                    First = rolls[0].Emote,
                    Second = rolls[1].Emote,
                    Third = rolls[2].Emote,
                    Reward = null,
                    System = guildConfig.SlotsSystem
                });
            }

            var reward = first;
            if (rolls.All(e => e.IsWildcard))
            {
                var groups = rolls
                    .GroupBy(e => e.Emote)
                    .OrderByDescending(e => e.Count());
                if (groups.Count() == 3)
                {
                    reward = rolls[Random.Shared.Next(rolls.Length)];
                }
                else
                {
                    reward = groups.First().First();
                }
            }
            else
            {
                reward = rolls.First(e => !e.IsWildcard);
            }

            return new(true, null, new()
            {
                IsWin = true,
                First = rolls[0].Emote,
                Second = rolls[1].Emote,
                Third = rolls[2].Emote,
                Reward = reward,
                System = guildConfig.SlotsSystem
            });
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Unexpected error in {method}", nameof(RollSlotsAsync));
            return new(false, [$"An unexpected error occurred in {nameof(RollSlotsAsync)}", ex.Message]);
        }
    }
}
