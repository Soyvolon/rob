﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using ROBBot.Structures.Database;
using ROBBot.Structures.Discord;
using ROBBot.Structures.Result;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Services.Module;
public class ModuleService(
		IDbContextFactory<ApplicationDbContext> dbContextFactory,
		ILogger<ModuleService> logger
	) : IModuleService
{
	public async Task<ActionResult> ChangeModuleStatusAsync(ulong guild, GuildModules module, bool status)
	{
		try
		{
			await using var dbContext = await dbContextFactory.CreateDbContextAsync();

			var guildConfig = await dbContext.GetGuildConfigurationAsync(guild);

			if (status)
				guildConfig.EnabledModules |= module;
			else guildConfig.EnabledModules &= ~module;

			await dbContext.SaveChangesAsync();

			return new(true);
		}
		catch (Exception ex)
        {
            logger.LogError(ex, "Unexpected error in {method}", nameof(ChangeModuleStatusAsync));
            return new(false, [$"An unexpected error occurred in {nameof(ChangeModuleStatusAsync)}", ex.Message]);
		}
	}
}
