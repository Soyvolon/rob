﻿using ROBBot.Structures.Discord;
using ROBBot.Structures.Result;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Services.Module;
public interface IModuleService
{
	public Task<ActionResult> ChangeModuleStatusAsync(ulong guild, GuildModules module, bool status);
}
