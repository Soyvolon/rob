﻿using DSharpPlus;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;

using ROBBot.Structures.Database;
using ROBBot.Structures.Discord;
using ROBBot.Structures.Result;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace ROBBot.Services.Isekai;
public class IsekaiService(
		IDbContextFactory<ApplicationDbContext> dbContextFactory,
		DiscordClient discordClient,
		ILogger<IsekaiService> logger
	) : IIsekaiService
{
	public async Task<ActionResult> UpdateSettingsAsync(ulong guild, Action<IsekaiSystem> updates)
	{
		try
		{
			await using var dbContext = await dbContextFactory.CreateDbContextAsync();

			var guildConfig = await dbContext.GetGuildConfigurationAsync(guild);

			updates.Invoke(guildConfig.IsekaiSystem);

			await dbContext.SaveChangesAsync();

			return new(true);
		}
		catch (Exception ex)
        {
            logger.LogError(ex, "Unexpected error in {method}", nameof(UpdateSettingsAsync));
            return new(false, [$"An unexpected error occurred in {nameof(UpdateSettingsAsync)}", ex.Message]);
		}
	}

	public async Task<ActionResult<(bool enabled, IsekaiSystem cfg)>> GetSettingsAsync(ulong guild)
	{
		try
		{
			await using var dbContext = await dbContextFactory.CreateDbContextAsync();

			var guildConfig = await dbContext.GetGuildConfigurationAsync(guild);

			return new(true, null, (
				guildConfig.EnabledModules.HasFlag(GuildModules.Isekai),
				guildConfig.IsekaiSystem
			));
		}
		catch (Exception ex)
        {
            logger.LogError(ex, "Unexpected error in {method}", nameof(GetSettingsAsync));
            return new(false, [$"An unexpected error occurred in {nameof(GetSettingsAsync)}", ex.Message]);
		}
	}

	#region Runner
	private Timer StatusTimer { get; set; }
	private Random Random { get; set; }

#if DEBUG
	private const int RunnerSeconds = 5;
#else
	private const int RunnerSeconds = 60;
#endif

	private const int SystemDelayMills = 50;

	private readonly SemaphoreSlim _lock = new(1);

	public Task InitAsync()
	{
		// Run the timer.
		Random = new();
		StatusTimer = new(async (e) => await OnTimerTickAsync(), null, 
			TimeSpan.FromSeconds(RunnerSeconds), TimeSpan.FromSeconds(RunnerSeconds));

		logger.LogInformation("Enabled Isekai service.");

		return Task.CompletedTask;
	}

	private async Task OnTimerTickAsync()
	{
		await _lock.WaitAsync();
		await using var dbContext = await dbContextFactory.CreateDbContextAsync();

		logger.LogDebug("Checking Isekai service for new triggers.");

		try
		{
			var configs = await dbContext.DiscordGuildConfigurations
				.Where(e => e.EnabledModules.HasFlag(GuildModules.Isekai))
				.ToListAsync();

			foreach (var cfg in configs)
			{
				var sys = cfg.IsekaiSystem;
				if (!IsReady(sys))
					continue;

				if (discordClient.Guilds.TryGetValue(cfg.GuildId, out var guild))
				{
					logger.LogInformation("Isekai system triggered for {guild}", guild.Id);

					var pick = Random.Next(0, guild.MemberCount + 1);
					var nick = sys.NameList[Random.Next(0, sys.NameList.Count)];

					var members = guild.GetAllMembersAsync();

					var counter = 0;
					await foreach (var item in members)
					{
						if (counter++ >= pick && !item.IsBot)
						{
							try
							{
								await item.ModifyAsync(e => e.Nickname = nick);
								cfg.IsekaiSystem.LastTrigger = DateTime.UtcNow;
							}
							catch { }

							break;
						}
					}
				}

				await Task.Delay(TimeSpan.FromMilliseconds(SystemDelayMills));
			}
		}
		finally
		{
			await dbContext.SaveChangesAsync();
			_lock.Release();
		}
	}

	private bool IsReady(IsekaiSystem system)
	{
		if (system.NameList.Count == 0)
			return false;

		float rng = 1 + Random.Next(0, (int)Math.Ceiling(system.Randomness * 100)) / 100f;
		var checkTimer = TimeSpan.FromSeconds(system.TriggerTimer.TotalSeconds * rng);
		return (DateTime.UtcNow - system.LastTrigger) >= checkTimer;
	}
	#endregion
}
