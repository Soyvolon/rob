﻿using ROBBot.Structures.Discord;
using ROBBot.Structures.Result;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Services.Isekai;
public interface IIsekaiService
{
	public Task<ActionResult> UpdateSettingsAsync(ulong guild, Action<IsekaiSystem> updates);
	public Task<ActionResult<(bool enabled, IsekaiSystem cfg)>> GetSettingsAsync(ulong guild);

	#region Runner
	/// <summary>
	/// Starts the Isekai system. Should be run after discord is turned on.
	/// </summary>
	/// <returns>The task for this action.</returns>
	public Task InitAsync();
	#endregion
}
