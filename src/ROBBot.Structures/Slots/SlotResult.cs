﻿using ROBBot.Structures.Discord;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Structures.Slots;
public struct SlotResult
{
    public string First { get; set; }
    public string Second { get; set; }
    public string Third { get; set; }

    public bool IsWin { get; set; }
    public SlotEntry? Reward { get; set; }

    public SlotsSystem System { get; set; }
}
