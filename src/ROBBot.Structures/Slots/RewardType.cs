﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Structures.Slots;
public enum RewardType
{
    Role,
    Nickname,
    Timeout,
    Disconnect,
    Kick
}
