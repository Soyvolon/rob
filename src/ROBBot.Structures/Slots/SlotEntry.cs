﻿using ROBBot.Structures.Discord;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Structures.Slots;
public class SlotEntry : DataObject<Guid>
{
    public SlotsSystem? System { get; set; }
    public Guid SystemKey { get; set; }

    public string Emote { get; set; } = string.Empty;
    public int Weight { get; set; } = 5;
    public bool IsWildcard { get; set; } = false;

    public RewardType RewardType { get; set; }

    #region Rewards
    public int TimeoutSeconds { get; set; } = 60;
    public string NicknameGranted { get; set; } = string.Empty;
    public ulong RoleToggled { get; set; }
    public string KickMessage { get; set; } = string.Empty;
    #endregion

    /// <summary>
    /// Displayed when the reward is given successfully.
    /// </summary>
    public string OnSuccessMessage { get; set; } = string.Empty;
    /// <summary>
    /// Displayed when the reward is not given successfully (permission or person not in vc).
    /// </summary>
    public string OnFailureMessage { get; set; } = string.Empty;

    public string GetEntryInfo()
    {
        string msg = $"**Weight:** {Weight}\n**Wildcard:** {IsWildcard}";
        switch (RewardType)
        {
            case RewardType.Role:
                msg += $"Toggles role: <@{RoleToggled}>";
                break;
            case RewardType.Nickname:
                msg += $"Grants Nickname: {NicknameGranted}";
                break;
            case RewardType.Timeout:
                msg += $"Times someone out for: {TimeSpan.FromSeconds(TimeoutSeconds):g}";
                break;
            case RewardType.Disconnect:
                msg += "Disconnects someone from VC.";
                break;
            case RewardType.Kick:
                msg += $"Kicks someone from the server, sending them an invite link and this first: {KickMessage}";
                break;
        }

        return msg;
    }
}
