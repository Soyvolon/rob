namespace ROBBot.Structures.Result;

/// <summary>
/// Wraps a value in a class.
/// </summary>
/// <param name="value">The starting value.</param>
/// <typeparam name="T">The type of value.</typeparam>
public class ValueWrapper<T>(T value)
{
    /// <summary>
    /// The value.
    /// </summary>
    public T Value { get; set; } = value;
}
