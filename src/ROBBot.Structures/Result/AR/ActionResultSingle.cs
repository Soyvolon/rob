﻿using System.Diagnostics.CodeAnalysis;

namespace ROBBot.Structures.Result;

/// <summary>
/// An <see cref="ActionResult"/> that also returns a value.
/// </summary>
/// <typeparam name="T">The type of value that will be returned.</typeparam>
/// <remarks>
/// Create a new instance.
/// </remarks>
/// <param name="success">Sets <see cref="ActionResult.Success"/>.</param>
/// <param name="errors">Sets <see cref="ActionResult.Errors"/>.</param>
/// <param name="result">Sets <see cref="Result"/>.</param>
public class ActionResult<T>(bool success, List<string>? errors = null, T? result = default)
    : ActionResultJson(success, errors, result)
{
    /// <summary>
    /// The result when <see cref="ActionResult.Success"/> is true.
    /// </summary>
    public new T? Result { get; init; } = result;

    /// <summary>
    /// Gets the result of this action.
    /// </summary>
    /// <param name="errors">Any errors for this action.</param>
    /// <param name="result">The successful result of this action.</param>
    /// <returns>True if the action succeeded.</returns>
    public bool GetResult(
        [NotNullWhen(false)] out List<string>? errors,
        [NotNullWhen(true)] out T? result)
    {
        if (Success)
        {
            errors = null;
            result = Result ?? default;
        }
        else
        {
            errors = Errors ?? [];
            result = default;
        }

        return Success;
    }
}