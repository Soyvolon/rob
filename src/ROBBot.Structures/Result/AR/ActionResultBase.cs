using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace ROBBot.Structures.Result;

/// <summary>
/// A result of an action with error handling.
/// </summary>
/// <remarks>
/// Create a new instance.
/// </remarks>
/// <param name="success">Sets <see cref="Success"/>.</param>
/// <param name="errors">Sets <see cref="Errors"/>.</param>
[JsonDerivedType(typeof(ActionResultJson), 1)]
public class ActionResult(bool success, List<string>? errors = null)
{
    /// <summary>
    /// If true, the action succeeded.
    /// </summary>
    [JsonInclude]
    protected bool Success { get; init; } = success;
    /// <summary>
    /// Contains error messages when <see cref="Success"/> is false.
    /// </summary>
    [JsonInclude]
    protected List<string>? Errors { get; init; } = errors;

    /// <summary>
    /// Gets the result of this action.
    /// </summary>
    /// <param name="errors">Any errors for this action.</param>
    /// <returns>True if the action succeeded.</returns>
    public bool GetResult([NotNullWhen(false)] out List<string>? errors)
    {
        if (Success)
            errors = null;
        else
            errors = Errors ?? [];

        return Success;
    }
}

