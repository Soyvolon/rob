﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ROBBot.Structures.Migrations
{
    /// <inheritdoc />
    public partial class Slots : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SlotsSystem",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "TEXT", nullable: false),
                    GuildKey = table.Column<Guid>(type: "TEXT", nullable: false),
                    NiceTryMessage = table.Column<string>(type: "TEXT", nullable: false),
                    RollingEmote = table.Column<string>(type: "TEXT", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SlotsSystem", x => x.Key);
                    table.ForeignKey(
                        name: "FK_SlotsSystem_DiscordGuildConfigurations_GuildKey",
                        column: x => x.GuildKey,
                        principalTable: "DiscordGuildConfigurations",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SlotEntries",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "TEXT", nullable: false),
                    SystemKey = table.Column<Guid>(type: "TEXT", nullable: false),
                    Emote = table.Column<string>(type: "TEXT", nullable: false),
                    Weight = table.Column<int>(type: "INTEGER", nullable: false),
                    IsWildcard = table.Column<bool>(type: "INTEGER", nullable: false),
                    RewardType = table.Column<int>(type: "INTEGER", nullable: false),
                    TimeoutSeconds = table.Column<int>(type: "INTEGER", nullable: false),
                    NicknameGranted = table.Column<string>(type: "TEXT", nullable: false),
                    RoleToggled = table.Column<ulong>(type: "INTEGER", nullable: false),
                    KickMessage = table.Column<string>(type: "TEXT", nullable: false),
                    OnSuccessMessage = table.Column<string>(type: "TEXT", nullable: false),
                    OnFailureMessage = table.Column<string>(type: "TEXT", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SlotEntries", x => x.Key);
                    table.ForeignKey(
                        name: "FK_SlotEntries_SlotsSystem_SystemKey",
                        column: x => x.SystemKey,
                        principalTable: "SlotsSystem",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SlotEntries_SystemKey",
                table: "SlotEntries",
                column: "SystemKey");

            migrationBuilder.CreateIndex(
                name: "IX_SlotsSystem_GuildKey",
                table: "SlotsSystem",
                column: "GuildKey",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SlotEntries");

            migrationBuilder.DropTable(
                name: "SlotsSystem");
        }
    }
}
