﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ROBBot.Structures.Migrations
{
    /// <inheritdoc />
    public partial class Isekai : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DiscordGuildConfigurations",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "TEXT", nullable: false),
                    GuildId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    EnabledModules = table.Column<int>(type: "INTEGER", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscordGuildConfigurations", x => x.Key);
                    table.UniqueConstraint("AK_DiscordGuildConfigurations_GuildId", x => x.GuildId);
                });

            migrationBuilder.CreateTable(
                name: "IsekaiSystem",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "TEXT", nullable: false),
                    GuildKey = table.Column<Guid>(type: "TEXT", nullable: false),
                    TriggerTimer = table.Column<TimeSpan>(type: "TEXT", nullable: false),
                    LastTrigger = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Randomness = table.Column<float>(type: "REAL", nullable: false),
                    NameList = table.Column<string>(type: "TEXT", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IsekaiSystem", x => x.Key);
                    table.ForeignKey(
                        name: "FK_IsekaiSystem_DiscordGuildConfigurations_GuildKey",
                        column: x => x.GuildKey,
                        principalTable: "DiscordGuildConfigurations",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IsekaiSystem_GuildKey",
                table: "IsekaiSystem",
                column: "GuildKey",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IsekaiSystem");

            migrationBuilder.DropTable(
                name: "DiscordGuildConfigurations");
        }
    }
}
