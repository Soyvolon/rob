﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using ROBBot.Structures.Discord;
using ROBBot.Structures.Slots;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Structures.Database;
public class ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, ILogger<ApplicationDbContext> logger) : DbContext(options)
{
	public DbSet<DiscordGuildConfiguration> DiscordGuildConfigurations { get; internal set; }
	public DbSet<SlotEntry> SlotEntries { get; internal set; }

	public async Task<DiscordGuildConfiguration> GetGuildConfigurationAsync(ulong guild)
	{
		var guildConfig = await DiscordGuildConfigurations
			.Where(e => e.GuildId == guild)
			.FirstOrDefaultAsync();

		if (guildConfig is null)
		{
			guildConfig = new()
			{
				GuildId = guild,
				IsekaiSystem = new(),
				SlotsSystem = new(),
			};

			Add(guildConfig);
			await SaveChangesAsync();

			logger.LogInformation("Created new guild config for {Guild}", guild);
		}
		else
		{
			bool update = false;
			if (guildConfig.IsekaiSystem is null)
			{
				update = true;
				guildConfig.IsekaiSystem = new();
			}

			if (guildConfig.SlotsSystem is null)
			{
				update = true;
				guildConfig.SlotsSystem = new();
			}

			if (update)
			{
				await SaveChangesAsync();

                logger.LogInformation("Updated guild config for {Guild} to the latest systems.", guild);
            }
		}

		logger.LogDebug("Found config {@Config}", guildConfig);

		return guildConfig;
	}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		base.OnModelCreating(builder);

		builder.Entity<DiscordGuildConfiguration>(ctx =>
		{
			ctx.HasKey(e => e.Key);
			ctx.HasAlternateKey(e => e.GuildId);

			ctx.HasOne(p => p.IsekaiSystem)
				.WithOne(d => d.Guild)
				.HasForeignKey<IsekaiSystem>(d => d.GuildKey)
				.IsRequired();
			ctx.Navigation(e => e.IsekaiSystem)
				.AutoInclude(true);

            ctx.HasOne(p => p.SlotsSystem)
                .WithOne(d => d.Guild)
                .HasForeignKey<SlotsSystem>(d => d.GuildKey)
                .IsRequired();
            ctx.Navigation(e => e.SlotsSystem)
                .AutoInclude(true);
        });

		builder.Entity<IsekaiSystem>(ctx =>
		{
			ctx.HasKey(e => e.Key);
		});

		builder.Entity<SlotsSystem>(ctx =>
		{
			ctx.HasKey(e => e.Key);

			ctx.HasMany(e => e.SlotEntries)
				.WithOne(e => e.System)
				.HasForeignKey(e => e.SystemKey);
        });

		builder.Entity<SlotEntry>(ctx =>
		{
			ctx.HasKey(e => e.Key);
		});
	}
}
