﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Structures.Discord;
public class DiscordGuildConfiguration : DataObject<Guid>
{
	public ulong GuildId { get; set; }
	public GuildModules EnabledModules { get; set; } = GuildModules.Default;

	public required IsekaiSystem IsekaiSystem { get; set; }
	public required SlotsSystem SlotsSystem { get; set; }
}
