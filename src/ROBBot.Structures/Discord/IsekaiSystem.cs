﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Structures.Discord;
public class IsekaiSystem : DataObject<Guid>
{
	public DiscordGuildConfiguration? Guild { get; set; }
	public Guid GuildKey { get; set; }

	/// <summary>
	/// How often someone is Isekaied.
	/// </summary>
	public TimeSpan TriggerTimer { get; set; } = TimeSpan.Zero;
	/// <summary>
	/// The last time someone was Isekaied.
	/// </summary>
	public DateTime LastTrigger { get; set; } = DateTime.MinValue;
	/// <summary>
	/// Between 0 and 1. Determines the randomness for <see cref="TriggerTimer"/>.
	/// </summary>
	public float Randomness { get; set; } = 0.2f;
	/// <summary>
	/// The list of names someone can get Isekaied as. Otherwise,
	/// the https://animechan.xyz/docs#random-quote api will be used
	/// to get a random character.
	/// </summary>
	public List<string> NameList { get; set; } = [];
}
