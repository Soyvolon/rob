﻿using ROBBot.Structures.Slots;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Structures.Discord;
public class SlotsSystem : DataObject<Guid>
{
    public DiscordGuildConfiguration? Guild { get; set; }
    public Guid GuildKey { get; set; }

    public List<SlotEntry> SlotEntries { get; set; } = [];
    public string NiceTryMessage { get; set; } = "Better luck next time!";
    public string RollingEmote { get; set; } = ":question:";
}
