﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBBot.Structures.Discord;

[Flags]
public enum GuildModules : int
{
	None	= 0_0000_0000,
	Isekai	= 0_0000_0001,
	Slots	= 0_0000_0002,

	Default = None
}
